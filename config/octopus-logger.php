<?php

return [
    'enableGoogleLogger' => env('LOGGER_ENABLE', true),
    'loggerName'         => env('LOGGER_NAME', 'octopus-logger'),
    'keyFile'            => storage_path('auth/' . env('APP_ENV') . '/google-cloud-logging.json'),
    'resource'           => [
        "type"   => "generic_task",
        "labels" => [
            "location"  => env('LOGGER_LOCATION', 'generic-location'),
            "namespace" => env('LOGGER_NAMESPACE', 'generic-namespace'),
        ],
    ],
];
