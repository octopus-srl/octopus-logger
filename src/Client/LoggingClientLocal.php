<?php

namespace Octopus\Logger\Client;

use Octopus\Logger\Logger\LoggerLocal;

class LoggingClientLocal
{

    public function logger($name, array $options = [])
    {
        return new LoggerLocal($name, $options);
    }


}
