<?php

namespace Octopus\Logger;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider as SupportServiceProvider;

class ServiceProvider extends SupportServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/octopus-logger.php' => config_path('octopus-logger.php'),
        ], 'config');

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/octopus-logger.php', 'octopus-logger'
        );

    }

}
