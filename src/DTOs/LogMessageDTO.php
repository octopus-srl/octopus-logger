<?php

namespace Octopus\Logger\DTOs;

class LogMessageDTO
{
    /** string */
    public $class;
    /** string */
    public $function;
    /** string */
    public $message;

    /**
     * LogMessageDTO constructor.
     * @param $message
     * @param string $class
     * @param string $function
     */
    public function __construct(
        $message,
        string $class = '',
        string $function = ''
    ) {
        $this->class = $class;
        $this->function = $function;
        $this->message = is_array($message) ? json_encode($message) : $message;
    }

}
