<?php

namespace Octopus\Logger\Services;

use Octopus\Logger\DTOs\LogMessageDTO;

interface LoggingServiceInterface
{

    /**
     * @param LogMessageDTO $logMessageDTO
     * @param array $options
     */
    public function writeLog(LogMessageDTO $logMessageDTO, array $options = []);

}
