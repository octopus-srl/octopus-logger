<?php

namespace Octopus\Logger\Services;

use Google\Cloud\Logging\Logger;
use Google\Cloud\Logging\LoggingClient;
use Illuminate\Support\Facades\Cache;
use Octopus\Logger\Client\LoggingClientLocal;
use Octopus\Logger\DTOs\LogMessageDTO;

class GoogleCloudLoggingService implements LoggingServiceInterface
{
    /** @var LoggingClient|LoggingClientLocal  */
    public $loggingClient;
    public Logger $logger;

    /**
     * GoogleCloudLogginService constructor.
     */
    public function __construct()
    {

        if (env('LOGGER_ENABLE', true)) {

            $keyFile = Cache::get('logger_google_keys');

            if(empty($keyFile)) {
                $keyFile = [
                    "type"                        => "",
                    "project_id"                  => "",
                    "private_key_id"              => "",
                    "private_key"                 => "",
                    "client_email"                => "",
                    "client_id"                   => "",
                    "auth_uri"                    => "",
                    "token_uri"                   => "",
                    "auth_provider_x509_cert_url" => "",
                    "client_x509_cert_url"        => "",
                ];

                if (file_exists(config('octopus-logger.keyFile'))) {
                    $keyFile = json_decode(file_get_contents(config('octopus-logger.keyFile')), true);
                }

                Cache::put('logger_google_keys', $keyFile);
            }

            $this->loggingClient = new LoggingClient([
                'keyFile' => $keyFile,
            ]);

        } else {
            $this->loggingClient = new LoggingClientLocal();
        }

    }

    /**
     * @param LogMessageDTO $logMessageDTO
     * @param array         $options
     */
    public function writeLog(LogMessageDTO $logMessageDTO, array $options = [])
    {
      $severity = Logger::INFO;
      $location = config('octopus-logger.resource.labels.location');
      $namespace = config('octopus-logger.resource.labels.namespace');
      extract($options, EXTR_IF_EXISTS);

      $message = '[' . config('octopus-logger.resource.labels.namespace') . '] ';
      if (!empty($logMessageDTO->class) && !empty($logMessageDTO->function)) {
          $message .= $logMessageDTO->class . '@' . $logMessageDTO->function;
      }
      $message .= ' : ' . $logMessageDTO->message;

      $this->logger = $this->loggingClient->logger(
          config('octopus-logger.loggerName'),
          [
              'resource' => [
                  "type"   => config('octopus-logger.resource.type'),
                  "labels" => [
                      "location"  => $location,
                      "namespace" => $namespace,
                  ],
              ]
          ]
      );

      $this->logger->write($message, [
          'severity' => $severity,
      ]);

    }
}
