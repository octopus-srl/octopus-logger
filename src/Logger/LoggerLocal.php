<?php

namespace Octopus\Logger\Logger;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;

/**
 * Class LoggerLocal
 * Description ...
 *
 * @category Octo
 * @package  Octopus\Logger\Logger
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright All You Can Code 2021
 * @link     https://www.allyoucancode.it
 */
class LoggerLocal
{

    private $name;
    private $options;

    public function __construct(string $name, array $options = [])
    {
        $this->options = $options;
        $this->name = $name;
    }

    public function write($entry, array $options = [])
    {
        $severity = 200;
        extract($options, EXTR_IF_EXISTS);

        $location = Arr::get($this->options, 'resource.labels.location', 'location');
        $namespace = Arr::get($this->options, 'resource.labels.namespace', 'namespace');

        $entry = implode(' | ', [ $this->name, $location, $namespace]) . ' : ' . $entry;

        switch ($severity) {
            case 500:
                Log::error($entry);
                break;
            case 700:
                Log::alert($entry);
                break;
            case 100:
                Log::debug($entry);
                break;
            case 300:
                Log::notice($entry);
                break;
            case 400:
                Log::warning($entry);
                break;
            default:
                Log::info($entry);
                break;
        }
    }


}
